package net.welarson.protomap.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestPropUtils {

    @Test
    public void testGetPropFromGetter_getName() {
        final var prop = PropUtils.getPropFromGetter("getName");
        assertTrue(prop.isPresent());
        assertEquals("name", prop.get());
    }

    @Test
    public void testGetPropFromGetter_isName() {
        final var prop = PropUtils.getPropFromGetter("isName");
        assertTrue(prop.isPresent());
        assertEquals("name", prop.get());
    }

    @Test
    public void testGetPropFromSetter_setName() {
        final var prop = PropUtils.getPropFromSetter("setName");
        assertTrue(prop.isPresent());
        assertEquals("name", prop.get());
    }
}
