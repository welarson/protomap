package net.welarson.protomap;

import org.joor.CompileOptions;
import org.joor.Reflect;
import org.junit.jupiter.api.Test;

public class TestProtoMapProcessor {
  private final ProtoMapProcessor processor = new ProtoMapProcessor();

  @Test
  public void testBasicCompile() {
    final var content = """
                package net.welarson.protomap.test;
                
                import net.welarson.protomap.annotation.ProtoMap;
                import net.welarson.protomap.testmodel.SimpleSrc;
                import net.welarson.protomap.testmodel.SimpleDest;
                
                @ProtoMap
                public interface PmTestGood {
                    SimpleDest toDest(SimpleSrc src);
                }
                """;
    System.out.println("==== TestBasicCompile:");
    System.out.println(content);
    Reflect.compile("net.welarson.protomap.test.PmTestGood", content, new CompileOptions().processors(processor).options("-source", "11"));
  }

  @Test
  public void testProtoSimpleCompile() {
    final var content = """
                package net.welarson.protomap.test;
                
                import net.welarson.protomap.annotation.ProtoMap;
                import net.welarson.protomap.test.simple.SimpleStringDest;
                import net.welarson.protomap.test.simple.SimpleStringSrc;
                
                @ProtoMap
                public interface PmTestSimple {
                    SimpleStringDest toDest(SimpleStringSrc value);
                }
                """;
    System.out.println("==== TestProtoSimpleCompile:");
    System.out.println(content);
    Reflect.compile("net.welarson.protomap.test.PmTestSimple", content, new CompileOptions().processors(processor).options("-source", "11"));
  }

  @Test
  public void testProtoSimpleTypesCompile() {
    final var content = """
                package net.welarson.protomap.test;
                
                import net.welarson.protomap.annotation.ProtoMap;
                import net.welarson.protomap.test.simple.SimpleTypesDest;
                import net.welarson.protomap.test.simple.SimpleTypesSrc;
                
                @ProtoMap
                public interface PmTestSimple {
                    SimpleTypesDest toDest(SimpleTypesSrc value);
                }
                """;
    System.out.println("==== TestProtoSimpleCompile:");
    System.out.println(content);
    Reflect.compile("net.welarson.protomap.test.PmTestSimple", content, new CompileOptions().processors(processor).options("-source", "11"));
  }

  @Test
  public void testProtoSimpleToBeanCompile() {
    final var content = """
          package net.welarson.protomap.test;
                
                import net.welarson.protomap.annotation.ProtoMap;
                import net.welarson.protomap.test.simple.SimpleTypesSrc;
                import net.welarson.protomap.testmodel.SimpleTypesBean;
                
                @ProtoMap
                public interface PmTestSimple {
                    SimpleTypesBean toDest(SimpleTypesSrc value);
                }
        """;
    System.out.println("==== TestProtoSimpleToBeanCompile:");
    System.out.println(content);
    Reflect.compile("net.welarson.protomap.test.PmTestSimple", content, new CompileOptions().processors(processor).options("-source", "11"));
  }

  @Test
  public void testSimpleBeanToProtoCompile() {
    final var content = """
          package net.welarson.protomap.test;
                
                import net.welarson.protomap.annotation.ProtoMap;
                import net.welarson.protomap.test.simple.SimpleTypesDest;
                import net.welarson.protomap.testmodel.SimpleTypesBean;
                
                @ProtoMap
                public interface PmTestSimple {
                    SimpleTypesDest toDest(SimpleTypesBean value);
                }
        """;
    System.out.println("==== TestProtoSimpleToBeanCompile:");
    System.out.println(content);
    Reflect.compile("net.welarson.protomap.test.PmTestSimple", content, new CompileOptions().processors(processor).options("-source", "11"));
  }
}
