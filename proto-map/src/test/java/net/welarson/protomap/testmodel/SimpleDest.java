package net.welarson.protomap.testmodel;

public class SimpleDest {
    private String name;

    public SimpleDest() {
    }

    public SimpleDest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
