package net.welarson.protomap.testmodel;

public class SimpleTypesBean {
  private String strValue;
  private int intValue;
  private long longValue;
  private int uintValue;
  private long ulongValue;
  private boolean boolValue;
  private double doubleValue;

  public String getStrValue() {
    return strValue;
  }

  public SimpleTypesBean setStrValue(String strValue) {
    this.strValue = strValue;
    return this;
  }

  public int getIntValue() {
    return intValue;
  }

  public SimpleTypesBean setIntValue(int intValue) {
    this.intValue = intValue;
    return this;
  }

  public long getLongValue() {
    return longValue;
  }

  public SimpleTypesBean setLongValue(long longValue) {
    this.longValue = longValue;
    return this;
  }

  public int getUintValue() {
    return uintValue;
  }

  public SimpleTypesBean setUintValue(int uintValue) {
    this.uintValue = uintValue;
    return this;
  }

  public long getUlongValue() {
    return ulongValue;
  }

  public SimpleTypesBean setUlongValue(long ulongValue) {
    this.ulongValue = ulongValue;
    return this;
  }

  public boolean isBoolValue() {
    return boolValue;
  }

  public SimpleTypesBean setBoolValue(boolean boolValue) {
    this.boolValue = boolValue;
    return this;
  }

  public double getDoubleValue() {
    return doubleValue;
  }

  public SimpleTypesBean setDoubleValue(double doubleValue) {
    this.doubleValue = doubleValue;
    return this;
  }
}
