package net.welarson.protomap.converter;

public interface PMapConverter {

    int getPriority();

    boolean isMatch(String getterName, String sourceTypeName, String setterName, String destTypeName);
    
    String genAssign(String source, String getterName, String sourceTypeName, String setterName, String destTypeName);
}
