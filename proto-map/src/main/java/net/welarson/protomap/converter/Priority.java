package net.welarson.protomap.converter;

public class Priority {
    public static final int INTERNAL_CONVERT_LIMIT = 2000;
    public static final int INTERNAL_MAPPERS = 1900;
    public static final int INTERNAL_SAME_TYPE = 1000;
    public static final int INTERNAL_TO_STRING = 100;
}
