package net.welarson.protomap.converter.internal;

import static net.welarson.protomap.util.TypeUtils.TYPE_STRING;

import net.welarson.protomap.converter.PMapConverter;
import net.welarson.protomap.converter.Priority;
import net.welarson.protomap.util.CodeUtils;

public class ToString implements PMapConverter {

  @Override
  public int getPriority() {
    return Priority.INTERNAL_TO_STRING;
  }

  @Override
  public boolean isMatch(String getterName,
      String sourceTypeName,
      String setterName,
      String destTypeName) {
    return TYPE_STRING.equals(destTypeName);
  }

  @Override
  public String genAssign(String source,
      String getterName,
      String sourceTypeName,
      String setterName,
      String destTypeName) {
    return "( "
        + source
        + "."
        + CodeUtils.call(getterName)
        + " ? "
        + source
        + "."
        + CodeUtils.call(getterName)
        + "().toString() : null )";
  }
}
