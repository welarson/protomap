package net.welarson.protomap.converter.internal;

import net.welarson.protomap.converter.PMapConverter;
import net.welarson.protomap.converter.Priority;
import net.welarson.protomap.util.CodeUtils;
import net.welarson.protomap.util.TypeUtils;

public class SameType implements PMapConverter {
    @Override
    public int getPriority() {
        return Priority.INTERNAL_SAME_TYPE;
    }

    @Override
    public boolean isMatch(String getterName, String sourceTypeName, String setterName, String destTypeName) {
        return sourceTypeName.equals(destTypeName)
                && (TypeUtils.isImmutable(sourceTypeName) || TypeUtils.isPrimitive(sourceTypeName));
    }

    @Override
    public String genAssign(String source, String getterName, String sourceTypeName, String setterName, String destTypeName) {
        return source + "." + CodeUtils.call(getterName);
    }
}
