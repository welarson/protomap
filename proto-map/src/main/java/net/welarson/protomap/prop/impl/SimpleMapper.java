package net.welarson.protomap.prop.impl;

import net.welarson.protomap.util.model.MapperProperty;
import net.welarson.protomap.util.model.PropModel;
import net.welarson.protomap.prop.PropMapper;
import net.welarson.protomap.registry.Convert;

import java.util.HashSet;
import java.util.Set;

public class SimpleMapper implements PropMapper {
    private final Set<PropModel> setters = new HashSet<>();
    private final Set<PropModel> getters = new HashSet<>();

    @Override
    public void addSetter(PropModel prop) {
        setters.add(prop);
    }

    @Override
    public void addGetter(PropModel prop) {
        getters.add(prop);
    }

    @Override
    public Set<MapperProperty> getMappedProps() {
        final Set<MapperProperty> mappedProps = new HashSet<>();
        for (PropModel setter : setters) {
            for (PropModel getter : getters) {
                final var setterName = setter.getBaseName();
                final var getterName = getter.getBaseName();
                if (setterName.isPresent() && getterName.isPresent() && setterName.get().equalsIgnoreCase(getterName.get())) {
                    final var converter = Convert.INSTANCE.findConverter(
                            getter.getMethodName(),
                            getter.getMethodType(),
                            setter.getMethodName(),
                            setter.getMethodType());
                    converter.ifPresent(pMapConverter -> mappedProps.add(new MapperProperty(setterName.get(),
                            setter.getMethodName(),
                            getter.getMethodName(),
                            setter.getMethodType(),
                            getter.getMethodType(),
                            pMapConverter)));

                }
            }
        }
        return mappedProps;
    }
}
