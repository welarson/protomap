package net.welarson.protomap.prop;

import net.welarson.protomap.util.model.MapperProperty;
import net.welarson.protomap.util.model.PropModel;

import java.util.Set;

public interface PropMapper {
    void addSetter(PropModel prop);
    void addGetter(PropModel prop);

    Set<MapperProperty> getMappedProps();
}
