package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;

public interface Generator {
    StringBuilder open(StringBuilder sb, MapperModel model);
    default StringBuilder close(StringBuilder sb, MapperModel model) {return sb;}
}
