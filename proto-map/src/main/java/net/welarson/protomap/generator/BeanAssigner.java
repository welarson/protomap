package net.welarson.protomap.generator;

import net.welarson.protomap.converter.PMapConverter;
import net.welarson.protomap.util.PropUtils;
import net.welarson.protomap.util.TypeUtils;

public class BeanAssigner {
  public void generateAssignment(StringBuilder sb,
      boolean nullGuard,
      String target,
      String source,
      String getterName,
      String sourceTypeName,
      String setterName,
      String destTypeName,
      PMapConverter converter) {
    if (nullGuard && !TypeUtils.isPrimitive(sourceTypeName)) {
      final var propName = PropUtils.getPropFromGetter(getterName).orElseThrow(() -> new IllegalStateException("getterName must be a getter"));
      sb.append("    final var ").append(propName).append(" = ");
      sb.append(converter.genAssign(source, getterName, sourceTypeName, setterName, destTypeName)).append(";\n");
      sb.append("    if (").append(propName).append(" != null) {\n");
      sb.append("      ").append(target).append(".").append(setterName).append("(").append(propName).append(");\n");
      sb.append("    }\n");
    }
    else {
      sb.append("    ").append(target).append(".").append(setterName).append("(");
      sb.append(converter.genAssign(source, getterName, sourceTypeName, setterName, destTypeName));
      sb.append(");\n");
    }
  }
}
