package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;

public class PackageGenerator implements Generator {
    @Override
    public StringBuilder open(StringBuilder sb, MapperModel model) {
        sb.append("package ").append(model.getPackageName()).append(";\n");
        return sb;
    }
}
