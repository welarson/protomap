package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;

public class ClassGenerator implements Generator {
    @Override
    public StringBuilder open(StringBuilder sb, MapperModel model) {
        sb.append("public class ")
                .append(model.getInterfaceName())
                .append("Impl implements ")
                .append(model.getInterfaceName())
                .append(" {\n");
        return sb;
    }

    @Override
    public StringBuilder close(StringBuilder sb, MapperModel model) {
        sb.append("}\n");
        return sb;
    }
}
