package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;
import net.welarson.protomap.util.model.MapperProperty;
import net.welarson.protomap.util.model.PMapMethod;

public class ToProtoMethodsGenerator implements Generator {
  final private BeanAssigner assigner = new BeanAssigner();

  @Override
  public StringBuilder open(StringBuilder sb, MapperModel model) {
    if (model.getMethods() != null) {
      model.getMethods().forEach(method -> generateMethod(sb, method));
    }
    return sb;
  }

  private void generateMethod(StringBuilder sb, PMapMethod method) {
    sb.append("  @Override\n");
    sb.append("  public ").append(method.getDestType());
    sb.append(" ").append(method.getName()).append("(");
    sb.append(method.getSrcType()).append(" src) {\n");

    generateBuilder(sb, method);
    method.getProps().forEach(prop -> generateAssign(sb, prop));
    generateReturn(sb);

    sb.append("  }\n");
  }

  private void generateBuilder(StringBuilder sb, PMapMethod method) {
    sb.append("    ").append(method.getDestType()).append(".Builder builder = ");
    sb.append(method.getDestType()).append(".newBuilder();\n");
  }

  private void generateAssign(StringBuilder sb, MapperProperty prop) {
    assigner.generateAssignment(sb, true, "builder",
        "src",
        prop.getGetterName(),
        prop.getGetterType(),
        prop.getSetterName(),
        prop.getSetterType(),
        prop.getConverter());
  }

  private void generateReturn(StringBuilder sb) {
    sb.append("    return builder.build();\n");
  }
}
