package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;
import net.welarson.protomap.util.model.MapperProperty;
import net.welarson.protomap.util.model.PMapMethod;

public class ToBeanMethodsGenerator implements Generator {
    final private BeanAssigner assigner = new BeanAssigner();

    @Override
    public StringBuilder open(StringBuilder sb, MapperModel model) {
        if (model.getMethods() != null) {
            model.getMethods().forEach(method -> generateMethod(sb, method));
        }
        return sb;
    }

    private void generateMethod(StringBuilder sb, PMapMethod method) {
        sb.append("  @Override\n");
        sb.append("  public ").append(method.getDestType());
        sb.append(" ").append(method.getName()).append("(");
        sb.append(method.getSrcType()).append(" src) {\n");

        generateConstruction(sb, method);
        method.getProps().forEach(prop -> generateAssign(sb, prop));
        generateReturn(sb);

        sb.append("  }\n");
    }

    private void generateConstruction(StringBuilder sb, PMapMethod method) {
        sb.append("    final ").append(method.getSrcType()).append(" dest =");
        sb.append(" new ").append(method.getDestType()).append("();\n");
    }

    private void generateReturn(StringBuilder sb) {
        sb.append("    return dest;\n");
    }

    private void generateAssign(StringBuilder sb, MapperProperty prop) {
        assigner.generateAssignment(sb, false, "dest",
                "src",
                prop.getGetterName(),
                prop.getGetterType(),
                prop.getSetterName(),
                prop.getSetterType(),
                prop.getConverter());
    }
}
