package net.welarson.protomap.generator;

import java.util.List;
import net.welarson.protomap.util.model.MapperModel;
import org.apache.commons.collections4.iterators.ReverseListIterator;

public class ToProtoCodeGenerator {
  private final List<Generator> generators = List.of(
      new PackageGenerator(),
      new ImportGenerator(),
      new ClassGenerator(),
      new ToProtoMethodsGenerator()
  );

  public String generate(MapperModel model) {
    final StringBuilder builder = new StringBuilder();

    for (Generator generator : generators) {
      generator.open(builder, model);
    }

    final var reverseIterator = new ReverseListIterator<>(generators);
    while (reverseIterator.hasNext()) {
      reverseIterator.next().close(builder, model);
    }
    return builder.toString();
  }
}
