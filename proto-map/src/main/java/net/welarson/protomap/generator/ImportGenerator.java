package net.welarson.protomap.generator;

import net.welarson.protomap.util.model.MapperModel;
import net.welarson.protomap.util.TypeUtils;

public class ImportGenerator implements Generator {
    @Override
    public StringBuilder open(StringBuilder sb, MapperModel model) {
        sb.append("\n");
        for (String refClass : model.getRefClasses()) {
            if (!refClass.startsWith("java.lang") && !isInPackage(refClass, model.getPackageName())) {
                sb.append("import ").append(refClass).append(";\n");
            }
        }
        sb.append("\n");
        return sb;
    }

    private boolean isInPackage(String className, String packageName) {
        return TypeUtils.getPackage(className).equals(packageName);
    }
}
