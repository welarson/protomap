package net.welarson.protomap;

import com.google.auto.service.AutoService;
import net.welarson.protomap.generator.CodeGenerator;
import net.welarson.protomap.generator.ToProtoCodeGenerator;
import net.welarson.protomap.util.model.MapperModel;
import net.welarson.protomap.util.model.PMapMethod;
import net.welarson.protomap.util.model.PropModel;
import net.welarson.protomap.prop.PropMapper;
import net.welarson.protomap.prop.impl.SimpleMapper;
import net.welarson.protomap.util.PropUtils;
import net.welarson.protomap.util.ProtoUtils;
import net.welarson.protomap.util.TypeUtils;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@SupportedAnnotationTypes("net.welarson.protomap.annotation.ProtoMap")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
public class ProtoMapProcessor extends AbstractProcessor {
  final private CodeGenerator codeGenerator = new CodeGenerator();
  final private ToProtoCodeGenerator toProtoCodeGenerator = new ToProtoCodeGenerator();

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    for (TypeElement annotation : annotations) {
      for (Element element : roundEnv.getElementsAnnotatedWith(annotation)) {
        if (element instanceof final TypeElement typeElement) {
          final PropMapper propMapper = new SimpleMapper();
          final MapperModel mapperModel = new MapperModel();
          mapperModel.setPackageName(TypeUtils.getPackage(typeElement.getQualifiedName().toString()));
          mapperModel.setInterfaceName(typeElement.getSimpleName().toString());
          final var refClasses = new HashSet<String>();
          final var enclosed = element.getEnclosedElements();
          for (Element i : enclosed) {
            if (i instanceof ExecutableElement) {
              final var returnTypeOpt = getReturnType((ExecutableElement) i);
              if (returnTypeOpt.isEmpty()) {
                continue;
              }
              final var singleParamOpt = getSingleParam((ExecutableElement) i);
              if (singleParamOpt.isEmpty()) {
                continue;
              }
              final var returnType = returnTypeOpt.get();
              final var singleParam = singleParamOpt.get();

              final var methodModel = new PMapMethod();
              methodModel.setDestType(returnType.getSimpleName().toString());
              methodModel.setSrcType(singleParam.getSimpleName().toString());
              methodModel.setName(i.getSimpleName().toString());

              refClasses.add(returnType.getQualifiedName().toString());
              refClasses.add(singleParam.getQualifiedName().toString());

              if (ProtoUtils.isProto(returnType)) {
                final var destBuilderOpt = ProtoUtils.getBuilder(returnType);
                if (destBuilderOpt.isEmpty()) {
                  continue;
                }
                final var destBuilder = destBuilderOpt.get();
                findSetters(destBuilder, propMapper);
                findGetters(singleParam, propMapper);

                propMapper.getMappedProps().forEach(methodModel::addProp);
                mapperModel.setMethods(Set.of(methodModel));

                mapperModel.setRefClasses(refClasses);

                final var classFile = toProtoCodeGenerator.generate(mapperModel);
                System.out.println("\n==============================================================");
                System.out.println(" PROTO TO PROTO CONVERTER GENERATED");
                System.out.println("==============================================================");
                System.out.println(classFile);
                System.out.println("==============================================================\n");

              }
              else {
                if (!hasNoArgConstructor(returnType)) {
                  continue;
                }

                findSetters(returnType, propMapper);
                findGetters(singleParam, propMapper);

                propMapper.getMappedProps().forEach(methodModel::addProp);
                mapperModel.setMethods(Set.of(methodModel));

                mapperModel.setRefClasses(refClasses);

                final var classFile = codeGenerator.generate(mapperModel);
                System.out.println("\n==============================================================");
                System.out.println(" BEAN TO BEAN CONVERTER GENERATED");
                System.out.println("==============================================================");
                System.out.println(classFile);
                System.out.println("==============================================================\n");
              }
            }
          }
        }
      }
    }
    return false;
  }

  private boolean hasNoArgConstructor(TypeElement typeElement) {
    final var constructors = typeElement.getEnclosedElements();
    for (Element i : constructors) {
      if (i instanceof ExecutableElement && TypeUtils.isPublic(i)) {
        final var params = ((ExecutableElement) i).getParameters();
        if (i.getSimpleName().toString().equals("<init>") && params.size() == 0) {
          return true;
        }
      }
    }
    return false;
  }

  private Optional<TypeElement> getSingleParam(ExecutableElement element) {
    final var params = element.getParameters();
    if (params.size() != 1) {
      return Optional.empty();
    }
    final var paramType = params.get(0).asType();
    if (paramType instanceof DeclaredType) {
      final var paramElem = ((DeclaredType) paramType).asElement();
      if (paramElem instanceof TypeElement) {
        return Optional.of((TypeElement) paramElem);
      }
    }
    return Optional.empty();
  }

  private Optional<TypeElement> getReturnType(ExecutableElement element) {
    final var returnType = element.getReturnType();
    if (returnType instanceof DeclaredType) {
      final var returnElem = ((DeclaredType) returnType).asElement();
      if (returnElem instanceof TypeElement) {
        return Optional.of((TypeElement) returnElem);
      }
    }
    return Optional.empty();
  }

  private void findGetters(TypeElement element, PropMapper propMapper) {
    final var enclosed = element.getEnclosedElements();
    final var getters = enclosed.stream()
        .filter(PropUtils::isGetter)
        .map(ExecutableElement.class::cast)
        .toList();
    getters.forEach(i -> {
      processGetter(i).ifPresent(type
          -> propMapper.addGetter(new PropModel(i.getSimpleName().toString(), type)));
    });
  }

  private void findSetters(TypeElement element, PropMapper propMapper) {
    final var enclosed = element.getEnclosedElements();
    final var setters = enclosed.stream()
        .filter(PropUtils::isSetter)
        .map(ExecutableElement.class::cast)
        .toList();
    setters.forEach(i -> {
      propMapper.addSetter(new PropModel(i.getSimpleName().toString(), processSetter(i)));
    });
  }

  private Optional<String> processGetter(ExecutableElement element) {
    return TypeUtils.getterReturnType(element);
  }

  private String processSetter(ExecutableElement element) {
    final var params = element.getParameters();
    final var paramType = params.get(0).asType();
    return paramType.toString();
  }
}
