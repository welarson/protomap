package net.welarson.protomap.registry;

import java.util.HashSet;
import java.util.Set;

public class Config {
    public static final Config INSTANCE = new Config();

    private final Set<String> setterPrefixes = new HashSet<>();
    private final Set<String> getterPrefixes = new HashSet<>();

    private Config() {
        setterPrefixes.add("set");
        getterPrefixes.add("get");
        getterPrefixes.add("is");
    }

    public Set<String> getSetterPrefixes() {
        return setterPrefixes;
    }

    public Set<String> getGetterPrefixes() {
        return getterPrefixes;
    }
}
