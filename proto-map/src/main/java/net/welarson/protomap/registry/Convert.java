package net.welarson.protomap.registry;

import net.welarson.protomap.converter.PMapConverter;
import net.welarson.protomap.converter.internal.SameType;

import java.util.Optional;
import java.util.SortedSet;
import net.welarson.protomap.converter.internal.ToString;

public class Convert {
    public static final Convert INSTANCE = new Convert();

    private final SortedSet<PMapConverter> converters = new java.util.TreeSet<>((o1, o2) -> o2.getPriority() - o1.getPriority());

    private Convert() {
        register(new SameType());
        register(new ToString());
    }

    public void register(PMapConverter converter) {
        converters.add(converter);
    }

    public Optional<PMapConverter> findConverter(String getterName,
                                                 String sourceTypeName,
                                                 String setterName,
                                                 String destTypeName) {
        for (PMapConverter converter : converters) {
            if (converter.isMatch(getterName, sourceTypeName, setterName, destTypeName)) {
                return Optional.of(converter);
            }
        }
        return Optional.empty();
    }
}
