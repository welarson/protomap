package net.welarson.protomap.annotation;

import javax.annotation.processing.SupportedAnnotationTypes;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SupportedAnnotationTypes("ProtoMap")
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ProtoMap {
}
