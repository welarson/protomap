package net.welarson.protomap.util;

import java.util.Optional;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;

public class TypeUtils {
  public final static String TYPE_STRING = "java.lang.String";

  public static boolean isPrimitive(String typeName) {
    return "int".equals(typeName)
        || "long".equals(typeName)
        || "float".equals(typeName)
        || "double".equals(typeName)
        || "boolean".equals(typeName)
        || "char".equals(typeName)
        || "byte".equals(typeName)
        || "short".equals(typeName);
  }

  public static boolean isImmutable(String typeName) {
    return TYPE_STRING.equals(typeName);
  }

  public static String getPackage(String typeName) {
    int index = typeName.lastIndexOf(".");
    if (index == -1) {
      return "";
    }
    return typeName.substring(0, index);
  }

  public static boolean isPublic(Element element) {
    return element.getModifiers().stream()
        .anyMatch(modifier -> "public".equals(modifier.toString()));
  }

  public static Optional<String> getterReturnType(ExecutableElement element) {
    final var returnType = element.getReturnType();
    if (returnType instanceof DeclaredType) {
      final var returnElem = ((DeclaredType) returnType).asElement();
      if (returnElem instanceof TypeElement) {
        return Optional.of((TypeElement) returnElem)
            .map(TypeElement::getQualifiedName)
            .map(String::valueOf);
      }
    }
    else if (returnType instanceof PrimitiveType) {
      return Optional.of(returnType.toString());
    }
    return Optional.empty();
  }
}
