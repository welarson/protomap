package net.welarson.protomap.util.model;

import net.welarson.protomap.util.PropUtils;

import java.util.Optional;

public class PropModel {
    private final String methodName;
    private final String methodType;

    public PropModel(String methodName, String methodType) {
        this.methodName = methodName;
        this.methodType = methodType;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getMethodType() {
        return methodType;
    }

    public Optional<String> getBaseName() {
        return Optional.ofNullable(PropUtils.getPropFromGetter(methodName)
                .orElse(PropUtils.getPropFromSetter(methodName)
                        .orElse(null)));
    }
}
