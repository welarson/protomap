package net.welarson.protomap.util.model;

import java.util.Set;

public class MapperModel {
    private String packageName;
    private String interfaceName;
    private Set<String> refClasses;
    private Set<PMapMethod> methods;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public Set<String> getRefClasses() {
        return refClasses;
    }

    public void setRefClasses(Set<String> refClasses) {
        this.refClasses = refClasses;
    }

    public Set<PMapMethod> getMethods() {
        return methods;
    }

    public void setMethods(Set<PMapMethod> methods) {
        this.methods = methods;
    }
}
