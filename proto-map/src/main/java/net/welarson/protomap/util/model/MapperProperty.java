package net.welarson.protomap.util.model;

import net.welarson.protomap.converter.PMapConverter;

public class MapperProperty {
    private String name;
    private String setterName;
    private String getterName;
    private String setterType;
    private String getterType;
    private PMapConverter converter;

    public MapperProperty() {}

    public MapperProperty(String name,
                          String setterName,
                          String getterName,
                          String setterType,
                          String getterType,
                          PMapConverter converter) {
        this.name = name;
        this.setterName = setterName;
        this.getterName = getterName;
        this.setterType = setterType;
        this.getterType = getterType;
        this.converter = converter;
    }

    public String getName() {
        return name;
    }

    public MapperProperty setName(String name) {
        this.name = name;
        return this;
    }

    public String getSetterName() {
        return setterName;
    }

    public MapperProperty setSetterName(String setterName) {
        this.setterName = setterName;
        return this;
    }

    public String getGetterName() {
        return getterName;
    }

    public MapperProperty setGetterName(String getterName) {
        this.getterName = getterName;
        return this;
    }

    public String getSetterType() {
        return setterType;
    }

    public MapperProperty setSetterType(String setterType) {
        this.setterType = setterType;
        return this;
    }

    public String getGetterType() {
        return getterType;
    }

    public MapperProperty setGetterType(String getterType) {
        this.getterType = getterType;
        return this;
    }

    public PMapConverter getConverter() {
        return converter;
    }

    @Override
    public String toString() {
        return "MapperProperty{" + "name=" + name + ", setterName=" + setterName + ", getterName=" + getterName + ", setterType=" + setterType + ", getterType=" + getterType + '}';
    }
}
