package net.welarson.protomap.util.model;

import java.util.HashSet;
import java.util.Set;

public class PMapMethod {
    private String name;
    private String destType;
    private String srcType;
    private Set<MapperProperty> props = new HashSet<>();

    public String getName() {
        return name;
    }

    public PMapMethod setName(String name) {
        this.name = name;
        return this;
    }

    public String getDestType() {
        return destType;
    }

    public PMapMethod setDestType(String destType) {
        this.destType = destType;
        return this;
    }

    public String getSrcType() {
        return srcType;
    }

    public PMapMethod setSrcType(String srcType) {
        this.srcType = srcType;
        return this;
    }

    public Set<MapperProperty> getProps() {
        return props;
    }

    public void addProp(MapperProperty prop){
        props.add(prop);
    }
}
