package net.welarson.protomap.util;

import net.welarson.protomap.registry.Config;
import org.apache.commons.lang3.StringUtils;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import java.util.Optional;
import java.util.Set;

public class PropUtils {
    public static Optional<String> getPropFromGetter(String methodName) {
        return getProp(methodName, Config.INSTANCE.getGetterPrefixes());
    }

    public static Optional<String> getPropFromSetter(String methodName){
        return getProp(methodName, Config.INSTANCE.getSetterPrefixes());
    }

    public static boolean isGetter(Element element) {
        if (element instanceof final ExecutableElement executableElement) {
            return executableElement.getParameters().size() == 0
                    && getPropFromGetter(executableElement.getSimpleName().toString()).isPresent();
        }
        return false;
    }

    public static boolean isSetter(Element element) {
        if (element instanceof final ExecutableElement executableElement) {
            return getPropFromSetter(executableElement.getSimpleName().toString()).isPresent();
        }
        return false;
    }

    private static Optional<String> getProp(String methodName, Set<String> prefixes) {
        for (String prefix : prefixes) {
            if (methodName.startsWith(prefix) && methodName.length() > prefix.length()) {
                return Optional.of(StringUtils.uncapitalize(methodName.substring(prefix.length())));
            }
        }
        return Optional.empty();
    }
}
