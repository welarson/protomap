package net.welarson.protomap.util;

import java.util.Optional;
import javax.lang.model.element.TypeElement;

public class ProtoUtils {
  public static boolean isProto(TypeElement typeElement) {
    // V3 Protocol Buffer
    final var superClass = typeElement.getSuperclass();
    return superClass.toString().equals("com.google.protobuf.GeneratedMessageV3");
  }

  public static Optional<TypeElement> getBuilder(TypeElement protoType) {
    final var enclosed = protoType.getEnclosedElements();
    for (var element : enclosed) {
      if (element instanceof TypeElement && element.getSimpleName().toString().equals("Builder")) {
        return Optional.of((TypeElement) element);
      }
    }
    return Optional.empty();
  }
}
